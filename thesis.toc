\select@language {english}
\contentsline {chapter}{\numberline {1}Theoretical background}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Density Functional Theory and Beyond}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Hohenberg-Kohn Theorems}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Kohn-Sham Single Particle Picture}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Approximations on Exchange-Correlation Functional}{7}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Range Separated Functional}{9}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Time Dependent Density Functional Theory}{11}{subsection.1.1.5}
\contentsline {subsubsection}{\numberline {1.1.5.1}Linear-Response Theory}{13}{subsubsection.1.1.5.1}
\contentsline {subsection}{\numberline {1.1.6}Implementation of DFT within Projected-Augment Wavefunction }{16}{subsection.1.1.6}
\contentsline {section}{\numberline {1.2}Molecular Spectroscopy }{17}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Introduction to Organic Molecules and Their Electronic Structure}{17}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Electronic Transitions}{21}{subsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.2.1}Singlet-Triplet States}{21}{subsubsection.1.2.2.1}
\contentsline {subsubsection}{\numberline {1.2.2.2}Photo-Absorption/Emission Processes}{23}{subsubsection.1.2.2.2}
\contentsline {subsection}{\numberline {1.2.3}Vibrational Properties}{26}{subsection.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.3.1}Infrared Spectroscopy}{27}{subsubsection.1.2.3.1}
\contentsline {subsubsection}{\numberline {1.2.3.2}Resonant Raman Spectroscopy}{30}{subsubsection.1.2.3.2}
\contentsline {section}{\numberline {1.3}Charge Transfer in Molecular System}{34}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Interactions Between Molecules}{34}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Marcus Theory of Charge Transfer}{37}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Constrained Density Functional Theory}{38}{subsection.1.3.3}
\contentsline {chapter}{\numberline {2}IR Spectroscopy of Triplet States in Acene Molecules}{43}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivation: Singlet-Fission Process}{44}{section.2.1}
\contentsline {section}{\numberline {2.2}Singlet Fission in Organic Molecules }{45}{section.2.2}
\contentsline {section}{\numberline {2.3}Triplet State Features in IR-spectra}{46}{section.2.3}
\contentsline {section}{\numberline {2.4}Computational Details}{50}{section.2.4}
\contentsline {chapter}{\numberline {3}Impact of Charges on Molecular Vibrational Frequencies}{51}{chapter.3}
\contentsline {section}{\numberline {3.1}Motivation: Why Blue/Red Shift in Molecular Vibrations?}{52}{section.3.1}
\contentsline {section}{\numberline {3.2}Modeling Fractional Charges }{52}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}An Example of F$_2$ Molecule}{57}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}An Example of C$_2$H$_2$ Molecule}{58}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}An Example of Benzene Molecule}{59}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Computational Details}{64}{section.3.3}
\contentsline {chapter}{\numberline {4}Detecting Charge Transfer Formation Between the Organic Semiconductor and Molecular Dopant Using Raman Shifts}{67}{chapter.4}
\contentsline {section}{\numberline {4.1}Motivation: Organic Donor-Acceptor Pairs}{68}{section.4.1}
\contentsline {section}{\numberline {4.2}Photo-Absorption Spectra}{69}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}CPDTBT Monomer}{69}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}CPDTBT Trimer}{71}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}CPDTBT in Contact to an Oxidizer}{73}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Raman Spectra of CPDTBT Monomer}{75}{section.4.3}
\contentsline {section}{\numberline {4.4}Observing CT Formation via Raman Shifts}{78}{section.4.4}
\contentsline {section}{\numberline {4.5}Computational Details}{82}{section.4.5}
\contentsline {chapter}{\numberline {5}Stability and IR Spectroscopy of $\beta $-Alanine Zwitterion in Water Clusters}{85}{chapter.5}
\contentsline {section}{\numberline {5.1}Motivation: Zwitterions in Amino Acids}{86}{section.5.1}
\contentsline {section}{\numberline {5.2}Gas-Phase $\beta $-Alanine}{87}{section.5.2}
\contentsline {section}{\numberline {5.3}$\beta $-Alanine Zwitterion Stability in Water}{91}{section.5.3}
\contentsline {section}{\numberline {5.4}$\beta $-Alanine Zwitterion IR-Spectroscopy}{96}{section.5.4}
\contentsline {section}{\numberline {5.5}Computational Details}{100}{section.5.5}
\contentsline {chapter}{\numberline {6}Conclusion and Outlook}{103}{chapter.6}
\contentsline {chapter}{Appendix}{107}{chapter.6}
\contentsline {chapter}{\numberline {A}Resonant Raman Spectra}{109}{appendix.A}
\contentsline {section}{\numberline {A.1}CPDTBT Mode Mixing with Oxidation}{109}{section.A.1}
\contentsline {section}{\numberline {A.2}Monomer Raman Spectra in all Wavelengths}{110}{section.A.2}
\contentsline {chapter}{\numberline {B}$\beta $-alanine and Zwitterionic form Geometries}{111}{appendix.B}
\contentsline {section}{\numberline {B.1}Structural parameters of low-energy $\beta $-alanine Conformers}{111}{section.B.1}
\contentsline {section}{\numberline {B.2}Comparison of Zwitterion Obtained from Conformers I and II}{117}{section.B.2}
\contentsline {section}{\numberline {B.3}Structural parameters of $\beta $-alanine zwitterion-(H$_2$O)$_n$ clusters }{121}{section.B.3}
\contentsline {chapter}{Abbreviations}{125}{appendix*.110}
