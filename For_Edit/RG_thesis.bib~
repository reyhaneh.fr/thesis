@ARTICLE{ase_vib,
       author = {{Frederiksen}, Thomas and {Paulsson}, Magnus and {Brandbyge}, Mads and
         {Jauho}, Antti-Pekka},
        title = "{Inelastic transport theory from first principles: Methodology and application to nanoscale devices}",
      journal = {Physical Review B},
     keywords = {72.10.-d, 63.22.+m, 71.15.-m, 73.23.-b, Theory of electronic transport, scattering mechanisms, Phonons or vibrational states in low-dimensional structures and nanoscale materials, Methods of electronic structure calculations, Electronic transport in mesoscopic systems, Condensed Matter - Mesoscopic Systems and Quantum Hall Effect},
         year = "2007",
        month = "May",
       volume = {75},
          eid = {205413},
        pages = {205413},
          doi = {10.1103/PhysRevB.75.205413},
archivePrefix = {arXiv},
       eprint = {cond-mat/0611562},
 primaryClass = {cond-mat.mes-hall},
       adsurl = {https://ui.adsabs.harvard.edu/\#abs/2007PhRvB..75t5413F},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}


@inproceedings{Walter-Raman,
  title={Ab-initio wavelength dependent Raman spectra : Placzek approximation and beyond},
  author={Michael Walter and Michael Moseler},
  year={2018}
}

@article{off-resonant-Raman,
  title = {Infrared intensities and Raman-scattering activities within density-functional theory},
  author = {Porezag, Dirk and Pederson, Mark R.},
  journal = {Physical Review B},
  volume = {54},
  issue = {11},
  pages = {7830--7836},
  numpages = {0},
  year = {1996},
  month = {Sep},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.54.7830},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.54.7830}
}


@article{born-Oppenheimer,
title={On the quantum theory of molecules},
author={Born, M and Oppenheimer, R},
journal={Annalen der Physik},
volume={84},
pages={457--484},
year={1927}
}

@article{HF0,
  title = {A Simplification of the Hartree-Fock Method},
  author = {Slater, J. C.},
  journal = {Physical Review},
  volume = {81},
  issue = {3},
  pages = {385--390},
  numpages = {0},
  year = {1951},
  month = {Feb},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRev.81.385},
  url = {https://link.aps.org/doi/10.1103/PhysRev.81.385}
}

@article{HF1,
title = "Self-consistent molecular Hartree—Fock—Slater calculations I. The computational procedure",
journal = "Chemical Physics",
volume = "2",
numbe
r = "1",
pages = "41 - 51",
year = "1973",
issn = "0301-0104",
doi = "https://doi.org/10.1016/0301-0104(73)80059-X",
url = "http://www.sciencedirect.com/science/article/pii/030101047380059X",
author = "E.J. Baerends and D.E. Ellis and P. Ros",
}

@article{HF2,
title = {Hartree--Fock method for atoms. A numerical approach},
author = {Fischer, C.F.},
abstractNote = {An integrated and consistent treatment of the Hartree--Fock approximation for bound states of atoms and a comparison of the numerous variants related to this approximation are presented: Koopmans' and Brillouin's theorems, the asymptotic form for large r, and relations among the various approximations. (JFP)},
doi = {},
journal = {},
number = ,
volume = ,
place = {United States},
year = {1977},
month = {1}
} 

@article{HF_bosons,
  title = {Ground State of the Electron Gas by a Stochastic Method},
  author = {Ceperley, D. M. and Alder, B. J.},
  journal = {Physical Review Letters},
  volume = {45},
  issue = {7},
  pages = {566--569},
  numpages = {0},
  year = {1980},
  month = {Aug},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.45.566},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.45.566}
}

@inbook{HF-correlation,
author = {Löwdin, Per-Olov},
publisher = {John Wiley and Sons, Ltd},
isbn = {9780470143483},
title = {Correlation Problem in Many-Electron Quantum Mechanics I. Review of Different Approaches and Discussion of Some Current Ideas},
booktitle = {Advances in Chemical Physics},
chapter = {},
pages = {207-322},
doi = {10.1002/9780470143483.ch7},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/9780470143483.ch7},
eprint = {https://onlinelibrary.wiley.com/doi/pdf/10.1002/9780470143483.ch7},
year = {2007},
keywords = {Schrödinger equation, coulomb correlation, correlation energy, Hartree-Fock scheme, electronic correlation},
abstract = {Summary This chapter contains sections titled: Introduction Formulation of the Correlation Problem Methods for Treating Electronic Correlation Recent Developments; Concluding Remarks}
}

@article{HF-Discussion,
  title = {Discussion on The Hartree-Fock Approximation},
  author = {Lykos, P. and Pratt, G. W.},
  journal = {Reviews of Modern Physics},
  volume = {35},
  issue = {3},
  pages = {496--501},
  numpages = {0},
  year = {1963},
  month = {Jul},
  publisher = {American Physical Society},
  doi = {10.1103/RevModPhys.35.496},
  url = {https://link.aps.org/doi/10.1103/RevModPhys.35.496}
}

@article{post-HF,
author = {Becke,Axel D. },
title = {Real-space post-Hartree–Fock correlation models},
journal = {The Journal of Chemical Physics},
volume = {122},
number = {6},
pages = {064101},
year = {2005},
doi = {10.1063/1.1844493},

URL = { 
        https://doi.org/10.1063/1.1844493
    
},
eprint = { 
        https://doi.org/10.1063/1.1844493
    
}

}


@article{MP,
author = {Binkley, J. S. and Pople, J. A.},
title = {Møller–Plesset theory for atomic ground state energies},
journal = {International Journal of Quantum Chemistry},
volume = {9},
number = {2},
pages = {229-236},
doi = {10.1002/qua.560090204},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/qua.560090204},
eprint = {https://onlinelibrary.wiley.com/doi/pdf/10.1002/qua.560090204},
abstract = {Abstract Mø–Plesset theory, in which electron correlation energy is calculated by perturbation techniques, is used in second order to calculate energies of the ground states of atoms up to neon. The unrestricted Hartree–Fock (UHF) Hamiltonian is used as the unperturbed system and the technique is then described as unrestricted Mø–Plesset to second order (UMP2). Use of large Gaussian basis sets suggests that the limiting UMP2 energies with a complete basis of s, p, and d functions account for 75–84\% of the correlation energy. Preliminary estimates of the contributions of basis functions with higher angular quantum numbers indicate that full UMP2 limits give even more accurate total energies.},,
year = {1975}
}

@article{MP2,
author = {Pople, J. A. and Krishnan, R. and Schlegel, H. B. and Binkley, J. S.},
title = {Derivative studies in hartree-fock and møller-plesset theories},
journal = {International Journal of Quantum Chemistry},
volume = {16},
number = {S13},
pages = {225-241},
doi = {10.1002/qua.560160825},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/qua.560160825},
eprint = {https://onlinelibrary.wiley.com/doi/pdf/10.1002/qua.560160825},
abstract = {Abstract The complete spin-orbital formulation of the analytical first and second derivatives of the Hartree-Fock (HF) energy as well as the analytical first derivative of the correlated second-order Møller-Plesset perturbation energy (MP2) is presented. Some features of an efficient computational method to calculate these derivatives are described. The methods are applied to calculate the harmonic vibrational frequencies of ethylene, and the results are compared with experiment.},,
year = {1979}
}

@article{CI,
author = {Pople,John A.  and Head‐Gordon,Martin  and Raghavachari,Krishnan },
title = {Quadratic configuration interaction. A general technique for determining electron correlation energies},
journal = {The Journal of Chemical Physics},
volume = {87},
number = {10},
pages = {5968-5975},
year = {1987},
doi = {10.1063/1.453520},

URL = { 
        https://doi.org/10.1063/1.453520
    
},
eprint = { 
        https://doi.org/10.1063/1.453520
    
}

}

@article{CI2,
author = {Werner,Hans‐Joachim  and Knowles,Peter J. },
title = {An efficient internally contracted multiconfiguration–reference configuration interaction method},
journal = {The Journal of Chemical Physics},
volume = {89},
number = {9},
pages = {5803-5814},
year = {1988},
doi = {10.1063/1.455556},

URL = { 
        https://doi.org/10.1063/1.455556
    
},
eprint = { 
        https://doi.org/10.1063/1.455556
    
}

}

@article{CC,
author = {Monkhorst, Hendrik J.},
title = {Calculation of properties with the coupled-cluster method},
journal = {International Journal of Quantum Chemistry},
volume = {12},
number = {S11},
pages = {421-432},
doi = {10.1002/qua.560120850},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/qua.560120850},
eprint = {https://onlinelibrary.wiley.com/doi/pdf/10.1002/qua.560120850},
abstract = {Abstract The cluster-expansion approach to the correlation problem, pioneered by Cocster, Kümmel, Cizek and Paldus, is extended to calculation of static and dynamic properties of many-fermion systems. Linear, inhomogeneous equations are obtained for properties of any order. A time-dependent formulation gives frequency-dependent properties, yielding excitation energies, transition probabilities, and (possibly) life times reminiscent of Green's function methods.},,
year = {1977}
}

@article{CC2,
author = {Stanton,John F.  and Bartlett,Rodney J. },
title = {The equation of motion coupled‐cluster method. A systematic biorthogonal approach to molecular excitation energies, transition probabilities, and excited state properties},
journal = {The Journal of Chemical Physics},
volume = {98},
number = {9},
pages = {7029-7039},
year = {1993},
doi = {10.1063/1.464746},

URL = { 
        https://doi.org/10.1063/1.464746
    
},
eprint = { 
        https://doi.org/10.1063/1.464746
    
}

}

@article{MP-SizeLimitation,
author = {Ayala,Philippe Y.  and Scuseria,Gustavo E. },
title = {Linear scaling second-order Moller–Plesset theory in the atomic orbital basis for large molecular systems},
journal = {The Journal of Chemical Physics},
volume = {110},
number = {8},
pages = {3660-3671},
year = {1999},
doi = {10.1063/1.478256},

URL = { 
        https://doi.org/10.1063/1.478256
    
},
eprint = { 
        https://doi.org/10.1063/1.478256
    
}

}

@Article{Häser1993,
author="H{\"a}ser, Marco",
title="M{\o}ller-Plesset (MP2) perturbation theory for large molecules",
journal="Theoretica chimica acta",
year="1993",
month="Nov",
day="01",
volume="87",
number="1",
pages="147--173",
abstract="A novel formulation of MP2 theory is presented which starts from the Laplace transform MP2 ansatz, and subsequently moves from a molecular orbital (MO) representation to an atomic orbital (AO) representation. Consequently, the new formulation is denoted AO-MP2. As in traditional MP2 approaches electron repulsion integrals still need to be transformed. Strict bounds on the individual MP2 energy contribution of each intermediate four-index quantity allow to screen off numerically insignificant integrals with a single threshold parameter. Implicit in our formulation is a bound to two-particle density matrix elements. For small molecules the computational cost for AO-MP2 calculations is about a factor of 100 higher than for traditional MO-based approaches, but due to screening the computational effort in larger systems will only grow with the fourth power of the size of the system (or less) as is demonstrated both in theory and in application. MP2 calculations on (non-metallic) crystalline systems seem to be a feasible extension of the Laplace transform approach. In large molecules the AO-MP2 ansatz allows massively parallel MP2 calculations without input/output of four-index quantities provided that each processor has in-core memory for a limited number of two-index quantities. Energy gradient formulas for the AO-MP2 approach are derived.",
issn="1432-2234",
doi="10.1007/BF01113535",
url="https://doi.org/10.1007/BF01113535"
}


@article{CI-SizeLimitation,
author = {Duch,Wl/odzisl/aw  and Diercksen,Geerd H. F. },
title = {Size‐extensivity corrections in configuration interaction methods},
journal = {The Journal of Chemical Physics},
volume = {101},
number = {4},
pages = {3018-3030},
year = {1994},
doi = {10.1063/1.467615},

URL = { 
        https://doi.org/10.1063/1.467615
    
},
eprint = { 
        https://doi.org/10.1063/1.467615
    
}

}

@article{Hohenberg-Kohn,
  title = {Inhomogeneous Electron Gas},
  author = {Hohenberg, P. and Kohn, W.},
  journal = {Physical Review},
  volume = {136},
  issue = {3B},
  pages = {B864--B871},
  numpages = {0},
  year = {1964},
  month = {Nov},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRev.136.B864},
  url = {https://link.aps.org/doi/10.1103/PhysRev.136.B864}
}


@article{ConceptualDFT,
author = {Geerlings, P. and De Proft, F. and Langenaeker, W.},
title = {Conceptual Density Functional Theory},
journal = {Chemical Reviews},
volume = {103},
number = {5},
pages = {1793-1874},
year = {2003},
doi = {10.1021/cr990029p},
    note ={PMID: 12744694},

URL = { 
        https://doi.org/10.1021/cr990029p
    
},
eprint = { 
        https://doi.org/10.1021/cr990029p
    
}

}

@article{Parr-Yang,
author = {Parr, Robert G. and Yang, Weitao},
title = {Density-Functional Theory of the Electronic Structure of Molecules},
journal = {Annual Review of Physical Chemistry},
volume = {46},
number = {1},
pages = {701-728},
year = {1995},
doi = {10.1146/annurev.pc.46.100195.003413},
    note ={PMID: 24341393},

URL = { 
        https://doi.org/10.1146/annurev.pc.46.100195.003413
    
},
eprint = { 
        https://doi.org/10.1146/annurev.pc.46.100195.003413
    
}
,
    abstract = { Recent fundamental advances in the density-functional theory of electronic structure are summarized. Emphasis is given to four aspects of the subject: (a) tests of functionals, (b) new methods for determining accurate exchange-correlation functionals, (c) linear scaling methods, and (d) developments in the description of chemical reactivity. }
}

@BOOK{Parr-YangBook,
  TITLE = {Density-Functional Theory of Atoms and Molecules},
  AUTHOR = {Robert G. Parr, Yang Weitao},
  YEAR = {1989}, % I looked it up
  PUBLISHER = {Oxford University Press},
}

@BOOK{TF,
  TITLE = {Theory of the Inhomogeneous Electron Gas},
  AUTHOR = {Stig Lundqvist, Norman H. March},
  YEAR = {1983}, % I looked it up
  PUBLISHER = {Springer Science+Business Media},
}

@article{Kohn-NobelLecture,
  title = {Nobel Lecture: Electronic structure of matter---wave functions and density functionals},
  author = {Kohn, W.},
  journal = {Rev. Mod. Phys.},
  volume = {71},
  issue = {5},
  pages = {1253--1266},
  numpages = {0},
  year = {1999},
  month = {Oct},
  publisher = {American Physical Society},
  doi = {10.1103/RevModPhys.71.1253},
  url = {https://link.aps.org/doi/10.1103/RevModPhys.71.1253}
}


@article{SCF,
author = {Blinder,S. M. },
title = {Basic Concepts of Self-Consistent-Field Theory},
journal = {American Journal of Physics},
volume = {33},
number = {6},
pages = {431-443},
year = {1965},
doi = {10.1119/1.1971665},

URL = { 
        https://doi.org/10.1119/1.1971665
    
},
eprint = { 
        https://doi.org/10.1119/1.1971665
    
}

}


@article{PULAY-densityMixing,
title = "Convergence acceleration of iterative sequences. the case of scf iteration",
journal = "Chemical Physics Letters",
volume = "73",
number = "2",
pages = "393 - 398",
year = "1980",
issn = "0009-2614",
doi = "https://doi.org/10.1016/0009-2614(80)80396-4",
url = "http://www.sciencedirect.com/science/article/pii/0009261480803964",
author = "Péter Pulay",
abstract = "Based on a recent method of Pople et al for the solution of large systems of linear equations, a procedure is given for accelerating the convergence of slowly converging quasi-Newton— Raphson type algorithms. This procedure is particularly advantageous if the number of parameters is so large that the calculation and storage of the hessian is no longer practical. Application to the SCF problem is treated in detail."
}

@article{PhysRevB.54.11169,
  title = {Efficient iterative schemes for ab initio total-energy calculations using a plane-wave basis set},
  author = {Kresse, G. and Furthm\"uller, J.},
  journal = {Phys. Rev. B},
  volume = {54},
  issue = {16},
  pages = {11169--11186},
  numpages = {0},
  year = {1996},
  month = {Oct},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.54.11169},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.54.11169}
}

@article{YukawaPotensial,
author = {Rawls,John M.  and Schulz,Michael },
title = {Energy Levels in a Yukawa Potential},
journal = {American Journal of Physics},
volume = {33},
number = {6},
pages = {444-445},
year = {1965},
doi = {10.1119/1.1971669},

URL = { 
        https://doi.org/10.1119/1.1971669
    
},
eprint = { 
        https://doi.org/10.1119/1.1971669
    
}

}

@article{XC_energy,
author = {Gritsenko,O. V.  and Schipper,P. R. T.  and Baerends,E. J. },
title = {Exchange and correlation energy in density functional theory: Comparison of accurate density functional theory quantities with traditional Hartree–Fock based ones and generalized gradient approximations for the molecules Li2, N2, F2},
journal = {The Journal of Chemical Physics},
volume = {107},
number = {13},
pages = {5007-5015},
year = {1997},
doi = {10.1063/1.474864},

URL = { 
        https://doi.org/10.1063/1.474864
    
},
eprint = { 
        https://doi.org/10.1063/1.474864
    
}

}

@article{LDA,
author = {Becke,Axel D. },
title = {A new mixing of Hartree–Fock and local density‐functional theories},
journal = {The Journal of Chemical Physics},
volume = {98},
number = {2},
pages = {1372-1377},
year = {1993},
doi = {10.1063/1.464304},

URL = { 
        https://doi.org/10.1063/1.464304
    
},
eprint = { 
        https://doi.org/10.1063/1.464304
    
}

}

@article{GGA-PBE,
  title = {Generalized Gradient Approximation Made Simple},
  author = {Perdew, John P. and Burke, Kieron and Ernzerhof, Matthias},
  journal = {Phys. Rev. Lett.},
  volume = {77},
  issue = {18},
  pages = {3865--3868},
  numpages = {0},
  year = {1996},
  month = {Oct},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.77.3865},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.77.3865}
}

@article{PBE0,
author = {Perdew,John P.  and Ernzerhof,Matthias  and Burke,Kieron },
title = {Rationale for mixing exact exchange with density functional approximations},
journal = {The Journal of Chemical Physics},
volume = {105},
number = {22},
pages = {9982-9985},
year = {1996},
doi = {10.1063/1.472933},

URL = { 
        https://doi.org/10.1063/1.472933
    
},
eprint = { 
        https://doi.org/10.1063/1.472933
    
}

}

@article{BLYP,
  title = {Development of the Colle-Salvetti correlation-energy formula into a functional of the electron density},
  author = {Lee, Chengteh and Yang, Weitao and Parr, Robert G.},
  journal = {Physical Review B},
  volume = {37},
  issue = {2},
  pages = {785--789},
  numpages = {0},
  year = {1988},
  month = {Jan},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.37.785},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.37.785}
}

@article{BLYP-2,
  title = {Density-functional exchange-energy approximation with correct asymptotic behavior},
  author = {Becke, A. D.},
  journal = {Physical Review A},
  volume = {38},
  issue = {6},
  pages = {3098--3100},
  numpages = {0},
  year = {1988},
  month = {Sep},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevA.38.3098},
  url = {https://link.aps.org/doi/10.1103/PhysRevA.38.3098}
}

@article{GGAFunctionlaReview,
author = {Finley, James},
year = {2003},
month = {08},
pages = {},
title = {Using the local density approximation and the LYP, BLYP, and B3LYP functionals within Reference--State One--Particle Density--Matrix Theory},
volume = {102},
journal = {Molecular Physics},
doi = {10.1080/00268970410001687452}

}

@article{PhysRevLett.91.146401,
  title = {Climbing the Density Functional Ladder: Nonempirical Meta--Generalized Gradient Approximation Designed for Molecules and Solids},
  author = {Tao, Jianmin and Perdew, John P. and Staroverov, Viktor N. and Scuseria, Gustavo E.},
  journal = {Physical Review Letters},
  volume = {91},
  issue = {14},
  pages = {146401},
  numpages = {4},
  year = {2003},
  month = {Sep},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.91.146401},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.91.146401}
}


@article{SDF,
  title = {Exchange and correlation in atoms, molecules, and solids by the spin-density-functional formalism},
  author = {Gunnarsson, O. and Lundqvist, B. I.},
  journal = {Phys. Rev. B},
  volume = {13},
  issue = {10},
  pages = {4274--4298},
  numpages = {0},
  year = {1976},
  month = {May},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.13.4274},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.13.4274}
}

@article{SIE,
author = {Mori-Sánchez,Paula  and Cohen,Aron J.  and Yang,Weitao },
title = {Many-electron self-interaction error in approximate density functionals},
journal = {The Journal of Chemical Physics},
volume = {125},
number = {20},
pages = {201102},
year = {2006},
doi = {10.1063/1.2403848},

URL = { 
        https://doi.org/10.1063/1.2403848
    
},
eprint = { 
        https://doi.org/10.1063/1.2403848
    
}

}

@article{SEI-fractionalCharge,
author = {Zhang,Yingkai  and Yang,Weitao },
title = {A challenge for density functionals: Self-interaction error increases for systems with a noninteger number of electrons},
journal = {The Journal of Chemical Physics},
volume = {109},
number = {7},
pages = {2604-2608},
year = {1998},
doi = {10.1063/1.476859},

URL = { 
        https://doi.org/10.1063/1.476859
    
},
eprint = { 
        https://doi.org/10.1063/1.476859
    
}

}

@article{SIE-ChargeTransfer,
  title = {Self-Interaction Errors in Density-Functional Calculations of Electronic Transport},
  author = {Toher, C. and Filippetti, A. and Sanvito, S. and Burke, Kieron},
  journal = {Phys. Rev. Lett.},
  volume = {95},
  issue = {14},
  pages = {146402},
  numpages = {4},
  year = {2005},
  month = {Sep},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.95.146402},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.95.146402}
}

@phdthesis{Rolf-Thesis,
  address = {Freiburg, Germany},
  title = {{Berechnung optischer Spektren und Grundzustandseigenschaften
neutraler und geladener Molek{\"u}le mittels Dichtefunktionaltheorie}},
  doi = {10.6094/UNIFR/11315},
  timestamp = {2017-02-15T16:24:40Z},
  school = {Universit{\"a}t Freiburg},
  author = {W{\"u}rdemann, Rolf},
  month = jul,
  year = {2016}
}

@article{RSF,
author = {Seth, Michael and Ziegler, Thomas and Steinmetz, Marc and Grimme, Stefan},
year = {2013},
month = {03},
pages = {2286},
title = {Modeling Transition Metal Reactions with Range-Separated Functionals},
volume = {9},
journal = {Journal of Chemical Theory and Computation},
doi = {10.1021/ct301112m}

}

@article{Rolf-RSF,
author = {Würdemann, Rolf and Walter, Michael},
title = {Charge Transfer Excitations with Range Separated Functionals Using Improved Virtual Orbitals},
journal = {Journal of Chemical Theory and Computation},
volume = {14},
number = {7},
pages = {3667-3676},
year = {2018},
doi = {10.1021/acs.jctc.8b00238},
    note ={PMID: 29894183},
URL = { 
        https://doi.org/10.1021/acs.jctc.8b00238 
},
eprint = { 
        https://doi.org/10.1021/acs.jctc.8b00238    
}

}

@article {Cohen_2008,
	author = {Cohen, Aron J. and Mori-S{\'a}nchez, Paula and Yang, Weitao},
	title = {Insights into Current Limitations of Density Functional Theory},
	volume = {321},
	number = {5890},
	pages = {792--794},
	year = {2008},
	doi = {10.1126/science.1158722},
	publisher = {American Association for the Advancement of Science},
	issn = {0036-8075},
	URL = {http://science.sciencemag.org/content/321/5890/792},
	eprint = {http://science.sciencemag.org/content/321/5890/792.full.pdf},
	journal = {Science}
}

@article{Vasiliev_2002,
  title = {First-principles density-functional calculations for optical spectra of clusters and nanocrystals},
  author = {Vasiliev, Igor and \"O\ifmmode \breve{g}\else \u{g}\fi{}\"ut, Serdar and Chelikowsky, James R.},
  journal = {Physical Review B},
  volume = {65},
  issue = {11},
  pages = {115416},
  numpages = {18},
  year = {2002},
  month = {Mar},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.65.115416},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.65.115416}
}

@article{QMC,
author = {Caffarel,Michel  and Claverie,Pierre },
title = {Development of a pure diffusion quantum Monte Carlo method using a full generalized Feynman–Kac formula. I. Formalism},
journal = {The Journal of Chemical Physics},
volume = {88},
number = {2},
pages = {1088-1099},
year = {1988},
doi = {10.1063/1.454227},

URL = { 
        https://doi.org/10.1063/1.454227
    
},
eprint = { 
        https://doi.org/10.1063/1.454227
    
}

}

@article{GW,
	doi = {10.1088/0034-4885/61/3/002},
	url = {https://doi.org/10.1088%2F0034-4885%2F61%2F3%2F002},
	year = 1998,
	month = {mar},
	publisher = {{IOP} Publishing},
	volume = {61},
	number = {3},
	pages = {237--312},
	author = {F Aryasetiawan and O Gunnarsson},
	title = {{TheGWmethod}},
	journal = {Reports on Progress in Physics},

	annote = {Calculations of ground-state and excited-state properties of materials have been one of the major goals of condensed matter physics. Ground-state properties of solids have been extensively investigated for several decades within the standard density functional theory. Excited-state properties, on the other hand, were relatively unexplored in  ab initio calculations until a decade ago. The most suitable approach up to now for studying excited-state properties of extended systems is the Green function method. To calculate the Green function one requires the self-energy operator which is non-local and energy dependent. In this article we describe the GW approximation which has turned out to be a fruitful approximation to the self-energy. The Green function theory, numerical methods for carrying out the self-energy calculations, simplified schemes, and applications to various systems are described. Self-consistency issue and new developments beyond the GW approximation are also discussed as well as the success and shortcomings of the GW approximation.}

}

@article{RGT,
  title = {Density-Functional Theory for Time-Dependent Systems},
  author = {Runge, Erich and Gross, E. K. U.},
  journal = {Physical Review Letters},
  volume = {52},
  issue = {12},
  pages = {997--1000},
  numpages = {0},
  year = {1984},
  month = {Mar},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.52.997},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.52.997}
}

@article{TDDFT-Michael,
author = {Walter,Michael  and Häkkinen,Hannu  and Lehtovaara,Lauri  and Puska,Martti  and Enkovaara,Jussi  and Rostgaard,Carsten  and Mortensen,Jens Jørgen },
title = {Time-dependent density-functional theory in the projector augmented-wave method},
journal = {The Journal of Chemical Physics},
volume = {128},
number = {24},
pages = {244101},
year = {2008},
doi = {10.1063/1.2943138},

URL = { 
        https://doi.org/10.1063/1.2943138
    
},
eprint = { 
        https://doi.org/10.1063/1.2943138
    
}

}

@article{lrTDDFT-Kohn,
  title = {Local density-functional theory of frequency-dependent linear response},
  author = {Gross, E. K. U. and Kohn, Walter},
  journal = {Phys. Rev. Lett.},
  volume = {55},
  issue = {26},
  pages = {2850--2852},
  numpages = {0},
  year = {1985},
  month = {Dec},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.55.2850},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.55.2850}
}


@BOOK{TDDFT-Gross,
  TITLE = {Time-Dependent Density Functional Theory},
  AUTHOR = {Miguel A.L. Marques, Carsten A. Ullrich, Fernando Nogueira, Angel Rubio, Kieron Burke, Eberhard K. U. Gross},
  YEAR = {2006}, % I looked it up
  PUBLISHER = {Springer},
}



@article{Excitation-Energies,
  title = {Excitation Energies from Time-Dependent Density-Functional Theory},
  author = {Petersilka, M. and Gossmann, U. J. and Gross, E. K. U.},
  journal = {Physical Review Letters},
  volume = {76},
  issue = {8},
  pages = {1212--1215},
  numpages = {0},
  year = {1996},
  month = {Feb},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.76.1212},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.76.1212}
}


@article{casida,
author = {Casida, Mark},
year = {2009},
month = {11},
pages = {3-18},
title = {Time-dependent density-functional theory for molecules and molecular solids},
volume = {914},
journal = {Journal of Molecular Structure: THEOCHEM},
doi = {10.1016/j.theochem.2009.08.018}
}



@article{gpaw1,
  title =	 {Real-space grid implementation of the projector
                  augmented wave method},
  author =	 {Mortensen, J. J. and Hansen, L. B. and Jacobsen,
                  K. W.},
  journal =	 {Physical Review B},
  volume =	 71,
  issue =	 3,
  pages =	 035109,
  numpages =	 11,
  year =	 2005,
  month =	 {Jan},
  publisher =	 {American Physical Society},
  doi =		 {10.1103/PhysRevB.71.035109},
  url =		 {http://link.aps.org/doi/10.1103/PhysRevB.71.035109}
}


@article{gpaw2,
	title = {Electronic structure calculations with {GPAW}: a real-space implementation of the projector augmented-wave method},
	volume = {22},
	issn = {0953-8984},
	shorttitle = {Electronic structure calculations with {GPAW}},
	url = {http://iopscience.iop.org/0953-8984/22/25/253202},
	doi = {10.1088/0953-8984/22/25/253202},
	abstract = {Electronic structure calculations have become an indispensable tool in many areas of materials science and quantum chemistry. Even though the Kohn–Sham formulation of the density-functional theory (DFT) simplifies the many-body problem significantly, one is still confronted with several numerical challenges. In this article we present the projector augmented-wave (PAW) method as implemented in the GPAW program package (https://wiki.fysik.dtu.dk/gpaw) using a uniform real-space grid representation of the electronic wavefunctions. Compared to more traditional plane wave or localized basis set approaches, real-space grids offer several advantages, most notably good computational scalability and systematic convergence properties. However, as a unique feature GPAW also facilitates a localized atomic-orbital basis set in addition to the grid. The efficient atomic basis set is complementary to the more accurate grid, and the possibility to seamlessly switch between the two representations provides great flexibility. While DFT allows one to study ground state properties, time-dependent density-functional theory (TDDFT) provides access to the excited states. We have implemented the two common formulations of TDDFT, namely the linear-response and the time propagation schemes. Electron transport calculations under finite-bias conditions can be performed with GPAW using non-equilibrium Green functions and the localized basis set. In addition to the basic features of the real-space PAW method, we also describe the implementation of selected exchange–correlation functionals, parallelization schemes, ΔSCF-method, x-ray absorption spectra, and maximally localized Wannier orbitals.},
	language = {en},
	number = {25},
	urldate = {2015-08-07},
	journal = {Journal of Physics: Condensed Matter},
	author = {Enkovaara, J. and Rostgaard, C. and Mortensen, J. J. and Chen, J. and Dułak, M. and Ferrighi, L. and Gavnholt, J. and Glinsvad, C. and Haikola, V. and Hansen, H. A. and Kristoffersen, H. H. and Kuisma, M. and Larsen, A. H. and Lehtovaara, L. and Ljungberg, M. and Lopez-Acevedo, O. and Moses, P. G. and Ojanen, J. and Olsen, T. and Petzold, V. and Romero, N. A. and Stausholm-Møller, J. and Strange, M. and Tritsaris, G. A. and Vanin, M. and Walter, M. and Hammer, B. and Häkkinen, H. and Madsen, G. K. H. and Nieminen, R. M. and Nørskov, J. K. and Puska, M. and Rantala, T. T. and Schiøtz, J. and Thygesen, K. S. and Jacobsen, K. W.},
	month = jun,
	year = {2010},
	pages = {253202},
	file = {Enkovaara et al. - 2010 - Electronic structure calculations with GPAW a rea.pdf:/home/mw/.zotero/zotero/n59glf08.default/zotero/storage/K35UNZ5E/Enkovaara et al. - 2010 - Electronic structure calculations with GPAW a rea.pdf:application/pdf;Snapshot:/home/mw/.zotero/zotero/n59glf08.default/zotero/storage/ZSDAK2AD/253202.html:text/html}
}


@BOOK{AnnaKohler_book,
  TITLE = {Electronic Processes in Organic Semiconductors: An Introduction},
  AUTHOR = {Anna Köhler, Heinz Bässler},
  YEAR = {2015}, 
  DOI = {10.1002/9783527685172}, 
  PUBLISHER = {Wiley‐VCH Verlag GmbH \& Co. KGaA},
}


@article{Rydberg_states,
  title =	 {The Rydberg States of Molecules.Parts I-V},
  author =	 { Robert S. Mulliken},
  journal =	 {Journal of the American Chemmical Society},
  volume =	 86,
  issue =	 16,
  year =	 1964,
  month =	 {August},
  publisher =	 {American Chemical Society},
 }


@BOOK{Sakurai,
  TITLE = {Modern Quantum Mechanics- Second Edition},
  AUTHOR = {J.J. Sakurai, Jim Napolitano},
  YEAR = {2011}, 
  ISBN = {978-0-8053-8291-4}, 
  PUBLISHER = {Addison-Wesley},
}



@article{Placzek_approx,
author = {Lee,Soo‐Y. },
title = {Placzek‐type polarizability tensors for Raman and resonance Raman scattering},
journal = {The Journal of Chemical Physics},
volume = {78},
number = {2},
pages = {723-734},
year = {1983},
doi = {10.1063/1.444775},

URL = { 
        https://doi.org/10.1063/1.444775
    
},
eprint = { 
        https://doi.org/10.1063/1.444775
    
}

}


@article{Albrecht_approx,
author = {Albrecht,Andreas C. },
title = {On the Theory of Raman Intensities},
journal = {The Journal of Chemical Physics},
volume = {34},
number = {5},
pages = {1476-1484},
year = {1961},
doi = {10.1063/1.1701032},

URL = { 
        https://doi.org/10.1063/1.1701032
    
},
eprint = { 
        https://doi.org/10.1063/1.1701032
    
}

}





